import { AlmacerviceAppPage } from './app.po';

describe('almacervice-app App', function() {
  let page: AlmacerviceAppPage;

  beforeEach(() => {
    page = new AlmacerviceAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
