import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutsComponent } from '../components/layouts/layouts.component';
import { LoginComponent } from '../components/login/login.component';
import { HomeComponent } from '../components/home/home.component';
import { VentasComponent } from '../components/ventas/ventas.component';
import { DashboardComponent} from '../components/dashboard/dashboard.component';
import { MantencionProductosComponent } from '../components/mantencion/productos/productos.component'
import { AuthGuard } from '../authentication/auth.guard';


const appRoutes: Routes = [
    // logged routes
    {
        
        //canActivate: [AuthGuard],
        children:[
            {
                //canActivate: [ AuthGuard ],
                path: 'home',
                component: HomeComponent   
            },
            {
                //canActivate: [ AuthGuard ],
                path: 'venta',
                component: VentasComponent   
            },
            {
                //canActivate: [ AuthGuard ],
                path: 'dashboard',
                component: DashboardComponent   
            },
            {
                //canActivate: [ AuthGuard ],
                path: 'productos',
                component: MantencionProductosComponent   
            },
        ],
        path: '',
        component: LayoutsComponent
    },
    // not logged routes
    {
        path: 'login',
        component: LoginComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);