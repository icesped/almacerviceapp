import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
    moduleId: module.id,
    selector: 'app-productos',
    templateUrl: 'productos.html',
    styleUrls: ['productos.sass']
})
export class MantencionProductosComponent implements OnInit {
    public selectedMenu: string = 'product'
  
    constructor(private titleService: Title) { }

    ngOnInit() {
        this.titleService.setTitle('Almacervice - Productos');  
    }

    toggleProductMenu(menu) {
        this.selectedMenu = menu
    }

}