import { Component } from '@angular/core';

@Component({
    selector: 'app-dashboard',
    template: ` <section class="component-body">
                    <div class="bread-box">
                        <a class="breadcrumb">Almacervice</a>
                        <a class="breadcrumb">Reportes</a>
                    </div>
                </section>`
})
export class DashboardComponent {
    constructor() { }

}