import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser'
import { User } from '../../shared/models/user';
import { isValidRut } from '../../shared/validations/rut.validation';
import { AuthService } from '../../services/auth.service';
declare var swal: any;

@Component({
    moduleId: module.id,
    selector: 'app-login',
    templateUrl: 'login.html',
    styleUrls: ['login.sass']
})

export class LoginComponent implements OnInit {
    private inputType: string = 'password';
    private usuario: User = new User()
    private tryLogin: boolean = false;

    // Objetos para validación
    formErrors = {
        'rut': '',
        'password': ''
    };

    validationMessages = {
        'rut': {
            'required': 'Debe ingresar su Rut.',
            'minlength': 'El Rut debe tener al menos 9 carácteres.',
            'isValidRut': 'El Rut ingresado no es válido o no cumple con el formato'
        },
        'password': {
            'required': 'Debe ingresar su Contraseña',
            'minlength': 'La Contraseña debe tener como mínimo 6 carácteres.',
        }
    };

    // Objeto Formulario
    loginForm: FormGroup;

    // Constructor
    constructor(
        private fb: FormBuilder,
        public authService: AuthService,
        public router: Router,
        private titleService: Title,
    ) { }

    // Se gatilla cuando inicializa el componente
    ngOnInit() {
        this.titleService.setTitle('Almacervice - Iniciar Sesión');
        this.buildForm();
    }

    // Define las validaciones del Form
    buildForm() {
        this.loginForm = this.fb.group({
            rut: [this.usuario.rut, Validators.compose([
                Validators.required,
                Validators.minLength(9),
                isValidRut()
            ])],
            password: [this.usuario.password, Validators.compose([
                Validators.required,
                Validators.minLength(6),
            ])]
        });

        // Cada vez que hay un cambio en el form llama a onValueChanged
        this.loginForm.valueChanges.subscribe(
            data => this.onValueChanged(data)
        );

        this.onValueChanged();
    }

    onLogin() {
        this.tryLogin = true;

        this.authService.login(this.usuario).subscribe(
            r => {
                this.router.navigate(['home']);
            },
            err => {
                swal(
                    'Ups! Tenemos un problema',
                    err,
                    'error'
                )
                this.tryLogin = false;
            });
    }

    // Cada vez que los valores cambian en el Form, se validan.
    onValueChanged(data?: any) {
        if (!this.loginForm) {
            return;
        }

        const form = this.loginForm;
        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);
            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }
}