import { Component, Input, OnInit } from '@angular/core';
import { DecimalsPipe } from '../../../shared/pipes/decimals.pipe'
import { ProductsService } from '../../../services/products.service'
import { Product } from '../../../shared/models/product'
import { UnitsService } from '../../../services/units.service';
import { CategoriesService } from '../../../services/categories.service';
import { ProvidersService } from '../../../services/providers.service';
import { MarginsService } from '../../../services/margins.service';
import { isGreaterThanZero } from '../../../shared/validations/greaterZero.validation';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
declare var swal: any;

@Component({
    moduleId: module.id,
    providers: [DecimalsPipe],
    selector: 'form-producto',
    templateUrl: 'producto-form.html',
    styleUrls: ['producto-form.sass']
})
export class FormularioProductoComponent implements OnInit {
    public productForm: FormGroup;
    public product: Product = new Product();
    public isDisabledProvider = false;
    public productsGrid = [];
    public units = [];
    public providers = [];
    public categories = [];
    public margins = [];
    public unidadMedidaFlag: boolean = false;
    public categoriaFlag: boolean = false;
    public proveedorFlag: boolean = false;
    public tipoMargenFlag: boolean = false;
    public isLoadingProducts: boolean = true;
    public isUpdate: boolean = false;

    constructor(
        private unitsService: UnitsService,
        private categoriesService: CategoriesService,
        private providersService: ProvidersService,
        private marginsService: MarginsService,
        private productsService: ProductsService,
        private fb: FormBuilder,
    ) { }

    ngOnInit() {
        this.buildForm();
        this.getAllUnits();
        this.getAllProviders();
        this.getAllCategories();
        this.getAllMargins();
        this.loadAllProducts();
    }

    // Objetos para validación
    formErrors = {
        'codigo': '',
        'detalle': '',
        'marca': '',
        'peso': '',
        'unidadMedida': '',
        'categoria': '',
        'proveedor': '',
        'stockCritico': '',
        'costo': '',
        'margen': '',
        'tipoMargen': ''
    };
    // Mensajes de validacion formulario.
    validationMessages = {
        'codigo': {
            'required': 'El Código del producto es obligatorio.',
            'isGreaterThanZero': 'El Código debe ser mayor a cero.'
        },
        'detalle': {
            'required': 'El Detalle es obligatorio.'
        },
        'marca': {
            'required': 'La Marca es obligatoria.'
        },
        'peso': {
            'required': 'El Peso del producto es obligatorio.',
            'isGreaterThanZero': 'El Peso debe ser mayor a cero.'
        },
        'unidadMedida': {},
        'categoria': {},
        'proveedor': {},
        'stockCritico': {
            'required': 'El Stock Crítico es obligatorio.',
            'isGreaterThanZero': 'El Stock Crítico debe ser mayor a cero.'
        },
        'costo': {
            'required': 'El Costo del producto es obligatorio.',
            'isGreaterThanZero': 'El Costo debe ser mayor a cero.'
        },
        'margen': {
            'required': 'El Margen de Ganancia es obligatorio.',
            'isGreaterThanZero': 'El Margen de Ganancia debe ser mayor a cero.'
        },
        'tipoMargen': {}
    };

    /**
     * Contruye el formulario y sus validaciones.
     */
    buildForm() {
        this.productForm = this.fb.group({
            codigo: [this.product.Codigo, Validators.compose([
                Validators.required,
                isGreaterThanZero()
            ])],
            detalle: [this.product.Detalle, Validators.compose([
                Validators.required
            ])],
            marca: [this.product.Marca, Validators.compose([
                Validators.required
            ])],
            peso: [this.product.Peso, Validators.compose([
                Validators.required,
                isGreaterThanZero()
            ])],
            unidadMedida: [this.product.UnidadMedidaId],
            categoria: [this.product.CategoriaId],
            proveedor: [this.product.ProveedorId],
            stockCritico: [this.product.StockCritico, Validators.compose([
                Validators.required
            ])],
            costo: [this.product.Costo, Validators.compose([
                Validators.required,
                isGreaterThanZero()
            ])],
            margen: [this.product.Margen, Validators.compose([
                Validators.required,
                isGreaterThanZero()
            ])],
            tipoMargen: [this.product.TipoMargenId]
        });

        // Cada vez que hay un cambio en el form llama a onValueChanged
        this.productForm.valueChanges.subscribe(
            data => this.onValueChanged(data)
        );

        this.onValueChanged();
    }

    /**
     * Cada vez que los valores cambian en el Form, se validan.
     */
    onValueChanged(data?: any) {
        if (!this.productForm) {
            return;
        }

        const form = this.productForm;
        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);
            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    /**
     * Obtiene todos los productos desde IndexedDB
     */
    loadAllProducts() {
        this.productsService.getAllProductsFromIndexedDB().then(
            products => {
                this.productsGrid = products;
                setTimeout(() => this.isLoadingProducts = false, 2000)
            }
        ).catch(
            err => {
                swal(
                    'Ups! Tenemos un problema',
                    'No fue posible cargar los productos: ' + err,
                    'error'
                )
                this.isLoadingProducts = false;
            }
            );
    }

    /**
     * Muetra el producto seleccionado de la grilla en el formulario.
     * @param product 
     */
    putProductOnForm(product) {
        this.product = product;
        this.isUpdate = true;
    }
    /**
     * Obtiene las unidades de medida
     */
    getAllUnits() {
        this.unitsService.getAllUnitsFromIndexedDB()
            .then(units => {
                this.units = units
            })
            .catch(err => {
                swal(
                    'Ups! Tenemos un problema',
                    'No fue posible cargar las Unidades de Medida: ' + err,
                    'error'
                )
            });
    }
    /**
     * Obtiene los proveedores
     */
    getAllProviders() {
        this.providersService.getAllProvidersFromIndexedDB()
            .then(providers => {
                this.providers = providers
            })
            .catch(err => {
                swal(
                    'Ups! Tenemos un problema',
                    'No fue posible cargar los Proveedores: ' + err,
                    'error'
                )
            });
    }

    /**
     * Obtiene las categorias
     */
    getAllCategories() {
        this.categoriesService.getAllCategoriesFromIndexedDB()
            .then(categories => {
                this.categories = categories
            })
            .catch(err => {
                swal(
                    'Ups! Tenemos un problema',
                    'No fue posible cargar las Categorías: ' + err,
                    'error'
                )
            });
    }

    /**
     * Obtiene todos los tipos de margenes de ganancia
     */
    getAllMargins() {
        this.marginsService.getAllMarginsFromIndexedDB()
            .then(margins => {
                this.margins = margins
            })
            .catch(err => {
                swal(
                    'Ups! Tenemos un problema',
                    'No fue posible cargar los Margenes de Ganancia: ' + err,
                    'error'
                )
            });
    }

    /**
     * Calcula el precio dependiendo el tipo de margen
     */
    getPriceByMarginType(value) {
        switch (String(value)) {
            // Si es %
            case '1':
                console.log("calculando %")
                this.product.Precio = Math.round(+this.product.Costo * ((+this.product.Margen / 100) + 1));
                this.tipoMargenFlag = false;
                break;
            // Si es $
            case '2':
                console.log("calculando $")
                this.product.Precio = +this.product.Costo + +this.product.Margen;
                this.tipoMargenFlag = false;
                break;
            default:
                this.product.Precio = 0;
                this.tipoMargenFlag = true;
                break;
        }
    }

    /**
     * Obtiene un producto Base desde el servidor
     * @param barcode 
     */
    getProductBase(barcode) {
        if (String(barcode).length >= 8) //Codigo de barra comienza desde 8 digitos.
        {
            this.productsService.getProductBaseFromServer(barcode).subscribe(
                res => {
                    this.product.Detalle = res.Detalle,
                        this.product.Marca = res.Marca,
                        this.product.Peso = res.Peso,
                        this.product.UnidadMedidaId = res.UnidadMedidaId
                },
                err => {
                    console.log("No fue posible encontrar el producto Base: " + err)
                }
            )
        }
    }

    /**
     * Crea un nuevo producto
     */
    addProduct() {
        if (this.validateSelects()) {
            this.productsService.createProduct(this.product).subscribe(
                res => {
                    swal(
                        'Producto Creado!',
                        'Tú producto ha sido creado correctamente!',
                        'success'
                    )
                    // Crea en IDB
                    console.log(res)
                    this.productsService.table.add(res)
                        .then(console.log("Guardado con exito en IDB"));
                    this.cleanProductForm()
                },
                err => {
                    swal(
                        'Ups! Tenemos un problema',
                        err,
                        'error'
                    )
                }
            )
        }
    }

    /**
     * Actualiza un producto.
     */
    updateProduct() {
        swal({
            title: `Se actualizará el Producto!`,
            text: "¿Estás seguro de actualizar el producto?",
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Actualizar'
        }).then(() => {
            this.productsService.updateProduct(this.product.Id, this.product).subscribe(
                res => {
                    // Obtiene el producto actualizado desde la BD para ser insertado en IDB
                    this.productsService.getProductFromServerById(this.product.Id).subscribe(
                        res => {
                            this.productsService.table.update(res.Codigo, {
                                Detalle: res.Detalle,
                                Marca: res.Marca,
                                Peso: res.Peso,
                                UnidadMedidaId: res.UnidadMedidaId,
                                CategoriaId: res.CategoriaId,
                                ProveedorId: res.ProveedorId,
                                StockCritico: res.StockCritico,
                                Costo: res.Costo,
                                Margen: res.Margen,
                                TipoMargenId: res.TipoMargenId,
                                Ganancia: res.Ganancia,
                                Precio: res.Precio,
                                FechaModificacion: res.FechaModificacion,
                                UsuarioModificador: res.UsuarioModificador,
                            }).then((updated) => {
                                if (updated) {
                                    this.isLoadingProducts = true;
                                    console.log(`Producto codigo: ${res.Codigo} actualizado.`);
                                }
                                else
                                    console.log(`No se realizo la actualización - No existe un producto con codigo ${res.Codigo}`);
                            }).catch(err => console.log(err));
                        },
                        err => {
                            console.log("Se ha producido un error al intentar obtener el producto.")
                        }
                    )
                    swal(
                        'Producto Actualizado!',
                        'Tú producto ha sido actualizado correctamente!',
                        'success'
                    ).then(() => this.cleanProductForm());
                },
                err => {
                    swal(
                        'Ups! Tenemos un problema',
                        'Tú producto no fue actualizado: ' + err,
                        'error'
                    ).then(() => this.cleanProductForm());
                }
            );
        });
    }

    /**
     * Elimina un producto dado su Id.
     */
    deleteProduct() {
        swal({
            title: `Se eliminará el Producto: ${this.product.Detalle}`,
            text: "¿Estás seguro de eliminar definitivamente el producto y sus registros de ventas?",
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Eliminar'
        }).then(() => {
            this.productsService.deleteProduct(this.product.Id).subscribe(
                res => {
                    // Elimina de IDB
                    this.productsService.table.delete(this.product.Codigo).then(() =>{
                        console.log("Producto eliminado")
                        this.isLoadingProducts = true;
                    })
                    swal(
                        'Producto Eliminado!',
                        'Tú producto ha sido eliminado correctamente!',
                        'success'
                    ).then(() => this.cleanProductForm());
                },
                err => {
                    swal(
                        'Ups! Tenemos un problema',
                        'Tú producto no fue eliminado: ' + err,
                        'error'
                    ).then(() => this.cleanProductForm());
                }
            );
        });
    }

    /**
     * Limpia el formulario
     */
    cleanProductForm() {
        this.loadAllProducts();
        this.productForm.reset();
        this.product.Precio = 0;
        this.isUpdate = false;
    }

    /**
     * Asegura que el combo Tipo de Margen sea Marcado cuando modificas el Costo o Margen.
     */
    detectChangeCostAndMargin() {
        this.product.TipoMargenId = 0;
        this.product.Precio = 0;
    }

    /**
     * Valida que se seleccionen los ComboBox
     */
    validateSelects() {
        if (!this.zeroOrNullValidator(this.product.UnidadMedidaId))
            this.unidadMedidaFlag = true;
        else
            this.unidadMedidaFlag = false;
        if (!this.zeroOrNullValidator(this.product.CategoriaId))
            this.categoriaFlag = true;
        else
            this.categoriaFlag = false;
        if (!this.zeroOrNullValidator(this.product.ProveedorId))
            this.proveedorFlag = true;
        else
            this.proveedorFlag = false;
        if (!this.zeroOrNullValidator(this.product.TipoMargenId))
            this.tipoMargenFlag = true;
        else
            this.tipoMargenFlag = false;

        return this.unidadMedidaFlag === false &&
            this.categoriaFlag === false &&
            this.proveedorFlag === false &&
            this.tipoMargenFlag === false ? true : false;
    }

    /**
     * Valida que los Select no sean tengan valor cero o null
     * @param value 
     */
    zeroOrNullValidator(value): boolean {
        let val = String(value)
        if (val == 'null')
            return false;
        else if (val === '0')
            return false;
        else
            return true;
    }

    /**
     * Valida que los los caracteres ingresados sean solo numeros o decimales
     * @param key 
     */
    decimalValidator(key) {
        //getting key code of pressed key
        var keycode = (key.which) ? key.which : key.keyCode;

        if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57))
            return false;
        else {
            var parts = key.srcElement.value.split('.');
            if (parts.length > 1 && keycode == 46)
                return false;
            return true;
        }
    }
}