import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../../services/auth.service';
declare var swal: any;
declare var moment: any;
moment.locale('es');

@Component({
    moduleId: module.id,
    selector: 'app-navbar',
    templateUrl: 'nav-bar.html',
    styleUrls: ['nav-bar.sass']

})
export class NavBarComponent {
    public clock = Observable.interval(1000).map(() => new moment().format('LTS a'));
    public date = moment().format('DD MMMM YYYY');
    public username: String = "Ignacio Céspedes Céspedes"
    public userProfile: String = "Administrador"
    public profileState: boolean = false

    constructor(private authService: AuthService) {
    }

    toggleProfileState() {
        this.profileState = this.profileState === false ? true : false
    }

    closeSesion() {
        this.profileState = this.profileState === false ? true : false
        swal({
            title: '¿Estás seguro de que quieres cerrar la sesión?',
            //text: "You won't be able to revert this!",
            type: 'question',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Cerrar Sesión'
        }).then(() => {
            this.authService.logout();
        })
    }
}