import { Component } from '@angular/core';
@Component({
    selector: 'app-big-spinner',
    template: `
  <div class="preloader-wrapper big-size active">
    <div class="spinner-layer">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div><div class="gap-patch">
        <div class="circle"></div>
      </div><div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>
  </div>   
    `,
    styles: [`
    .big-size {
      width: 100px;
      height: 100px;
      margin-bottom: 15px;
    }
    `]
})
export class BigLoaderSpinnerComponent {
 
    constructor() { }
}                   
                                
                                