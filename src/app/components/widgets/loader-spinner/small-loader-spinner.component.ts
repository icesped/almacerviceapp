import { Component } from '@angular/core';
@Component({
    selector: 'app-small-spinner',
    template: `
    <div class="preloader-wrapper small active">
        <div class="spinner-layer spinner-green-only">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>  
    `,
    styles: [`
    .small {
      width: 25px;
      height: 25px;
    }
    `]
})
export class SmallLoaderSpinnerComponent {
 
    constructor() { }
}                   
                                
                                