import { Component, HostListener } from '@angular/core';

@Component( {
    moduleId: module.id,
    selector: 'app-menu',
    styleUrls: ['menu.sass'],
    templateUrl: 'menu.html'
})

export class MenuComponent {
    public menuState: boolean = false;

    constructor(){}

    toggleMenu() {
        this.menuState = this.menuState === false ? true : false
    }

    closeMenu(){
        this.menuState = false;
    }

}