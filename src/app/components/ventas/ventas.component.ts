import { Component, OnInit, EventEmitter, HostListener } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { DecimalsPipe } from '../../shared/pipes/decimals.pipe';
import { ProductsService } from '../../services/products.service';
import { Sale } from '../../shared/models/sale';
import { MaterializeAction } from 'angular2-materialize';
import { Product } from '../../shared/models/product';
declare var swal: any;

@Component({
    moduleId: module.id,
    selector: 'app-ventas',
    providers: [DecimalsPipe],
    templateUrl: 'ventas.html',
    styleUrls: ['ventas.sass']
})


export class VentasComponent implements OnInit {
    // Lectura de codigo de barra
    public barcode;
    public pressedFlag = false;
    public barcodeChars = [];

    // Productos en venta
    public productsForSale = [];

    // Busqueda de producto
    public productsFinded = [];
    public productSearched: String = ""
    public boxProductsFindedFlag: boolean = false

    // Venta
    public sale = new Sale();

    // Modal Descuentos
    public modalActions = new EventEmitter<string | MaterializeAction>();

    constructor(
        private titleService: Title,
        private productsService: ProductsService) {
    }

    ngOnInit() {
        this.titleService.setTitle('Almacervice - Venta');
    }

    ngOnDestroy() {
        if (this.productsForSale.length > 0)
            confirm("Actualmente se encuentra en una venta, esta seguro que desea salir?")
    }

    /**
     *  Metodo encargado de leer el ingreso de datos del Scanner.
     */
    @HostListener('document:keypress', ['$event'])
    handleBarcodeInputEvent(event: KeyboardEvent): void {
        if (event.which >= 48 && event.which <= 57)
            this.barcodeChars.push(String.fromCharCode(event.which));

        if (this.pressedFlag == false) {

            setTimeout(() => {
                if (this.barcodeChars.length >= 8) {
                    // unión de chars para crear un string con el codigo de barra
                    this.barcode = this.barcodeChars.join("");

                    console.log("Barcode Scanned: " + this.barcode);
                    this.getProductByBarcode(this.barcode);
                }
                this.barcodeChars = [];
                this.pressedFlag = false;
            }, 200);
        }

        this.pressedFlag = true;
    }

    /**
     * Obtiene un producto por su codigo
     * @param barcode 
     */
    getProductByBarcode(barcode: string): void {

        // Valida si el producto existe actualmente en el proceso de venta
        if (this.productsForSale.length > 0) {
            var productExist = this.productsForSale.filter(p => p.Codigo == this.barcode)[0]
            if (productExist != undefined) {
                if (this.sumQuantityByProduct(productExist))
                    return
            }
        }

        // valida si existe en el origen
        this.productsService.getProductByBarcode(this.barcode).then(
            product => {
                if (product === undefined)
                    swal(
                        'Producto no encontrado',
                        'Para agregar el producto haga click Aquí: ',
                        'warning'
                    )
                else
                    this.addRowToSalesGrid(product)
            })
    }

/**
 * Borra un elemento de la lista de productos
 * @param product 
 */
deleteRowFromSalesGrid(product): void {
    var index = this.productsForSale.indexOf(product)
        if (index > -1)
            this.productsForSale.splice(index, 1)
}

/**
 * Agrega un elemento a la lista de productos
 * @param product 
 */
addRowToSalesGrid(product): void {
    this.productsForSale.push(product);
}

/**
 * Busca un producto por su nombre
 * @param event 
 */
searchProductsByName(event): void {
    let detail = String(event);

    if(detail != null && detail != "") {
        this.boxProductsFindedFlag = true;

        this.productsService.getProductByName(detail).then(products => this.productsFinded = products)
    }
        else {
        this.boxProductsFindedFlag = false;
        this.productsFinded = [];
    }
}

/**
 * Agrega un producto al detalle cuando es buscado por nombre
 * @param product 
 */
selectProductByName(product): void {
    this.boxProductsFindedFlag = false;
    this.productSearched = "";

    if(this.sumQuantityByProduct(product))
            return
        else
            this.addRowToSalesGrid(product);
}

/**
 * Aumenta la cantidad de un producto, solo si existe en la lista de compra
 * @param product 
 */
sumQuantityByProduct(product): Boolean {
    for (var i in this.productsForSale) {
        if (this.productsForSale[i].Codigo === product.codigo) {
            this.productsForSale[i].Cantidad++;
            return true;
        }
    }
    return false;
}

/**
 * Obtiene el total a pagar de un producto
 * @param cantidad 
 * @param precio 
 */
getTotalByProduct(product) {
    var quantity = parseFloat(product.cantidad).toFixed(1)
    var total = (+quantity * product.precio)

    return isNaN(+total) ? 0 : +total
}

/**
 * Valida que los los caracteres ingresados sean solo numeros o decimales
 * @param key 
 * @param product 
 */
validateDecimal(key) {
    //getting key code of pressed key
    var keycode = (key.which) ? key.which : key.keyCode;

    if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57))
        return false;
    else {
        var parts = key.srcElement.value.split('.');
        if (parts.length > 1 && keycode == 46)
            return false;
        return true;
    }

}

/**
 * Obtiene el subtotal de la venta
 */
getSubtotal() {
    var subtotal = 0
    this.productsForSale.forEach(p => {
        subtotal += p.Total
    });

    this.sale.subtotal = subtotal
    this.getTotal(this.sale.subtotal, this.sale.descuento)

    return isNaN(subtotal) ? 0 : subtotal
}

getTotal(subtotal, discount) {
    this.sale.total = (subtotal - discount)
}

/**
 * Abre el modal de descuentos
 */
openModalDiscount() {
    this.modalActions.emit({ action: "modal", params: ['open'] });
}
}