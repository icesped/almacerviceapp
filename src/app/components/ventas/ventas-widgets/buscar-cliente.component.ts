import { Component, EventEmitter } from '@angular/core';
import { MaterializeAction } from 'angular2-materialize';

@Component({
	moduleId: module.id,
	selector: 'buscar-cliente',
	templateUrl: 'buscar-cliente.html',
	styleUrls: ['buscar-cliente.sass']
})

export class BuscarClienteComponent {
	public clientSearched: String = "";
	public boxClientFindedFlag: boolean = false
	public clientsFinded: any = [];
	public modalActions = new EventEmitter<string|MaterializeAction>();

	constructor() { }


	searchClientByRut(event) {
		if (event != "") {
			this.boxClientFindedFlag = true;

			//this.productsService.getProductByName(event).then(products => this.productsFinded = products)
		}
		else {
			this.boxClientFindedFlag = false;
			this.clientsFinded = [];
		}
	}

	openModalAddClient()
	{
			this.modalActions.emit({action:"modal",params:['open']});
	}	
}