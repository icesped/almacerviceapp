import { Component, EventEmitter } from '@angular/core';
import { MaterializeAction } from 'angular2-materialize';
import { ProductsService } from '../../services/products.service';
import { CategoriesService } from '../../services/categories.service';
import { UnitsService } from '../../services/units.service';
import { ProvidersService } from '../../services/providers.service';
import { MarginsService } from '../../services/margins.service';
import { DexieService } from '../../services/dexie.service';

@Component({
    moduleId: module.id,
    selector: 'app-layouts',
    templateUrl: 'layouts.html',
    styleUrls: ['layouts.sass']
})

export class LayoutsComponent {
    public modalActions = new EventEmitter<string | MaterializeAction>();
    public hasAllLoaded: boolean = false;
    public loadProductsState: string = 'loading';
    public loadCategoriesState: string = 'loading';
    public loadUnitsState: string = 'loading';
    public loadProvidersState: string = 'loading';
    public loadMarginsState: string = 'loading';


    constructor(
        private dexieService: DexieService,
        private productsService: ProductsService,
        private categoriesService: CategoriesService,
        private unitsService: UnitsService,
        private providersService: ProvidersService,
        private marginsService: MarginsService
    ) {
        if (!dexieService.isOpen())
            dexieService.open();
        setTimeout(() => {
            this.LoadProductsFromServer();
            this.LoadCategoriesFromServer();
            this.LoadUnitsFromServer();
            this.LoadProvidersFromServer();
            this.LoadMarginsFromServer();
        }, 2000)
    }

    /**
     * Abre el modal con el detalle de las cargas
     */
    openModal() {
        this.modalActions.emit({ action: "modal", params: ["open"] });
    }

    /**
     * Carga los todos los productos de la sucursal
     */
    LoadProductsFromServer(): void {
        this.productsService.getAllProductsFromServer().subscribe(
            products => {
                if(products.length > 0)
                {
                    products.forEach(p => {
                        // Por cada producto del server, añadir a IndexedDB
                        this.productsService.table.add(p)
                            .then(() => {
                                this.loadProductsState = 'done';
                                console.log("Productos cargados con exito");
                            })
                            .catch(err => {
                                if (err.name == 'ConstraintError')
                                    this.loadProductsState = 'done';
                                else
                                    this.loadProductsState = 'error';
                                console.log("No fue posible cargar el producto: " + err);
                            })
                            .finally(() => this.checkAllLoaded());
                    });
                }
                else
                    this.loadProductsState = 'done'; //TODO: Empty
            },
            err => {
                console.log("No fue posible obtener los productos: " + err);
                this.loadProductsState = 'error';
            }
        )
    }

    /**
     * Carga todas las categorias
     */
    LoadCategoriesFromServer(): void {
        this.categoriesService.getAllCategoriesFromServer().subscribe(
            categories => categories.forEach(c => {
                this.categoriesService.table.add(c)
                    .then(() => {
                        this.loadCategoriesState = 'done'
                        console.log("Categorías cargadas con exito")
                    })
                    .catch(err => {
                        if (err.name == 'ConstraintError')
                            this.loadCategoriesState = 'done';
                        else
                            this.loadCategoriesState = 'error';
                        console.log("No fue posible cargar las categorias: " + err)
                    })
                    .finally(() => this.checkAllLoaded());
            }),
            err => {
                console.log("No fue posible obtener las categorias: " + err)
                this.loadCategoriesState = 'error'
            }
        )
    }
    /**
     * Carga todas las Unidades de Medida
     */
    LoadUnitsFromServer(): void {
        this.unitsService.getAllUnitsFromServer().subscribe(
            units => units.forEach(um => {
                this.unitsService.table.add(um)
                    .then(() => {
                        this.loadUnitsState = 'done';
                        console.log("Unidades de Medida cargadas con exito");
                    })
                    .catch(err => {
                        if (err.name == 'ConstraintError')
                            this.loadUnitsState = 'done';
                        else
                            this.loadUnitsState = 'error';
                        console.log("No fue posible cargar las Unidades de Medida: " + err)
                    })
                    .finally(() => this.checkAllLoaded());

            }),
            err => {
                console.log("No fue posible obtener las Unidades de Medida: " + err)
                this.loadUnitsState = 'error'
            }
        )
    }

    /**
     * Carga todos los Proveedores
     */
    LoadProvidersFromServer(): void {
        this.providersService.getAllProvidersFromServer().subscribe(
            providers => providers.forEach(prov => {
                this.providersService.table.add(prov)
                    .then(() => {
                        this.loadProvidersState = 'done';
                        console.log("Proveedores cargados con exito");
                    })
                    .catch(err => {
                        if (err.name == 'ConstraintError')
                            this.loadProvidersState = 'done';
                        else
                            this.loadProvidersState = 'error';
                        console.log("No fue posible cargar los Proveedores: " + err)
                    })
                    .finally(() => this.checkAllLoaded());
            }),
            err => {
                console.log("No fue posible obtener los Proveedores: " + err)
                this.loadProvidersState = 'error'
            }
        )
    }


    /**
     * Carga todos los Tipos de Margen
     */
    LoadMarginsFromServer(): void {
        this.marginsService.getAllMarginTypesFromServer().subscribe(
            margins => margins.forEach(m => {
                this.marginsService.table.add(m)
                    .then(() => {
                        this.loadMarginsState = 'done';
                        console.log("Tipos de Margen cargados con exito");
                    })
                    .catch(err => {
                        if (err.name == 'ConstraintError')
                            this.loadMarginsState = 'done';
                        else
                            this.loadMarginsState = 'error';
                        console.log("No fue posible cargar los Tipos de Margen: " + err)
                    })
                    .finally(() => this.checkAllLoaded());
            }),
            err => {
                console.log("No fue posible obtener los Tipos de Margen: " + err)
                this.loadMarginsState = 'error'
            }
        )
    }

    /***
     * Verifica que todos los items esten cargados.
     */
    checkAllLoaded() {
        if (this.loadProductsState != 'loading'
            && this.loadCategoriesState != 'loading'
            && this.loadUnitsState != 'loading'
            && this.loadProvidersState != 'loading'
            && this.loadMarginsState != 'loading')
            this.hasAllLoaded = true;
        else
            this.hasAllLoaded = false;
    }
}