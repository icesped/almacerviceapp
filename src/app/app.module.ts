import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterializeModule } from 'angular2-materialize';
import { Ng2Rut } from 'ng2-rut';
import { ChartsModule } from 'ng2-charts';

let modules = [
    HttpModule,
    BrowserModule,
    MaterializeModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2Rut,
    ChartsModule
]

// Components
import { AppComponent } from './app.component';
import { LayoutsComponent  } from './components/layouts/layouts.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { VentasComponent } from './components/ventas/ventas.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MantencionProductosComponent } from './components/mantencion/productos/productos.component';

let components = [
    AppComponent,
    LayoutsComponent,   
    HomeComponent,
    LoginComponent,
    VentasComponent,
    DashboardComponent,
    MantencionProductosComponent
];

// Import Widgets
import { BigLoaderSpinnerComponent } from './components/widgets/loader-spinner/big-loader-spinner.component';
import { SmallLoaderSpinnerComponent } from './components/widgets/loader-spinner/small-loader-spinner.component';
import { NavBarComponent } from './components/widgets/nav-bar/nav-bar.component';
import { MenuComponent } from './components/widgets/menu/menu.component';
import { BuscarClienteComponent } from './components/ventas/ventas-widgets/buscar-cliente.component';
import { FormularioProductoComponent } from './components/forms/products/producto-form.component';

let widgets = [
    BigLoaderSpinnerComponent,
    SmallLoaderSpinnerComponent,
    NavBarComponent,
    MenuComponent,
    BuscarClienteComponent,
    FormularioProductoComponent
];

// Import Pipes
import { ThousandsSeparatorPipe } from './shared/pipes/thousands.separator.pipe';
import { DecimalsPipe } from './shared/pipes/decimals.pipe';

let pipes = [
    ThousandsSeparatorPipe,
    DecimalsPipe
];

// Import directives
import { DecimalsFormatterDirective } from './shared/directives/decimalsFormatter.directive';
import { OnlyNumbersDirective } from './shared/directives/onlyNumbers.directive';

let directives = [
    DecimalsFormatterDirective,
    OnlyNumbersDirective
];

// Import Services
import { AuthService } from './services/auth.service';
import { AuthGuard } from './authentication/auth.guard';
import { AlertService } from './services/alert.service';
import { ProductsService } from './services/products.service';
import { DexieService } from './services/dexie.service';
import { CategoriesService } from './services/categories.service';
import { UnitsService } from './services/units.service';
import { ProvidersService } from './services/providers.service';
import { MarginsService } from './services/margins.service';


let services = [
    AuthService,
    AuthGuard,
    AlertService,
    ProductsService,
    DexieService,
    CategoriesService,
    UnitsService,
    ProvidersService,
    MarginsService
]

import { routing }  from './routes/app.routing';

@NgModule({
  declarations: [ ...components, ...widgets, ...pipes, ...directives ],
  imports: [ ...modules, routing ],
  providers: [ ...services ],
  bootstrap: [AppComponent]
})
export class AppModule { }