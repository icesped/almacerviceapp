import { Injectable } from '@angular/core'

declare var Materialize: any

@Injectable()
export class AlertService {
    private icon: String
    private style: String = "rounded"
    constructor() {
     }

    showAlert(title: String, subtitle?: String, type?: String, duration?: Number, styles?: String[], callback?: String) {
        // Valida tipo alerta
        if (type != undefined)
            switch (type) {
                case "warning":
                    this.icon = "warning"
                    this.style = `${this.style} orange darken-1`
                    break
                case "success":
                    this.icon = "check_circle"
                    this.style = `${this.style} light-green accent-3`
                    break
                case "info":
                    this.icon = "info"
                    this.style = `${this.style} light-blue darken-4`
                    break
                case "error":
                    this.icon = "report_problem"
                    this.style = `${this.style} red accent-4`
                    break
            }

        // Valida Css
        if (styles != undefined)
            this.style = `${this.style} ${styles}`

        var htmlStr = `
        <div class="alert-box">
          <div class="alert-icon">
            <i class="medium material-icons">${this.icon}</i>
          </div>
          <div class="alert-text center">
            <div class="alert-title">${title}</div>
            <div class="alert-content">${subtitle}</div>
          </div>
          <div class="alert-close">
            <a onclick="$('.rounded').fadeOut()"><i class="material-icons">close</i></a>
          </div>
        </div>
        `

        Materialize.toast(htmlStr, duration, this.style)
    }
}