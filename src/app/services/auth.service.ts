import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { DexieService } from '../services/dexie.service';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { User } from '../shared/models/user';
import '../services/rxjs/index';

@Injectable()
export class AuthService {
    private API_URI = environment.URI;
    public isLoggedIn: boolean = false;

    constructor(private http: Http, public router: Router, private dexieService: DexieService) { }

    /**
     * Valida las credenciales en el servidor y retorna un JWT.
     * @param usuario 
     */
    login(usuario: User): Observable<Response> {
        let rutDv = usuario.rut.replace('-', '');
        let rut = rutDv.slice(0, -1);
        let dv = rutDv.slice(-1).toUpperCase();

        let URL = this.API_URI + 'auth'
        let body = {
            rut: rut,
            dv: dv,
            password: usuario.password
        }

        let bodyJson = JSON.stringify(body);

        return this.http.post(URL, bodyJson, { headers: this.getHeaders() })
            .delay(2500)
            .map((res: Response) => {
                this.isLoggedIn = true;
                let data = this.extractData(res);
                window.localStorage.setItem('jwt', data.access_token);
            }).catch(err => this.handleError(err).do(res => this.isLoggedIn = false));
    }

    /**
     * Cierra la sesión del usuario, eliminando los datos de navegación.
     */
    logout() {
        this.isLoggedIn = false;
        window.localStorage.clear();
        this.dexieService.delete();
        this.router.navigate(['login']);
    }

    /**
     * Obtiene las cabeceras para realizar las peticiones.
     */
    private getHeaders() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return headers;
    }

    /**
      * Maneja los errores y genera logs.
      */
    //TODO: GENERAR LOGS.
    private handleError(error: any) {
        let errorBody = error.json();
        let errMsg = error.status === 0 ? 'No fue posible establecer conexión con el servidor.' : errorBody.message;
        console.error(`${error.status}-${error.statusText}: ${errorBody.message}`);

        return Observable.throw(errMsg);
    }

    /**
     * Extrae los datos de la respuesta obtenida por la API
     * @param res 
     */
    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }
}