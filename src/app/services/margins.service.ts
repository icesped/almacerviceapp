import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Product } from '../shared/models/product';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { environment } from '../../environments/environment';
import { DexieService } from '../services/dexie.service';

@Injectable()
export class MarginsService {
    public table;
    private API_URI = environment.URI;
    private token = localStorage.getItem('jwt');
    
    constructor(private http: Http, private dexieService: DexieService) {
        this.table = this.dexieService.table('tipoDeMargen');
    }

    public getAllMarginTypesFromServer() {
        let URL = this.API_URI + 'margenes';
        return this.http.get(URL, { headers: this.getHeaders() })
            .map(this.extractData)
            .catch(this.handleError);
    }

    /**
     * Obtiene todos los margenes almacenadas en IndexedDB
     */
    public getAllMarginsFromIndexedDB(){
        return this.table.toArray((margins) => margins);
    }

    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead

        return Observable.throw(errMsg);
    }

    /**
     * Extrae la data 
     * @param res 
     */
    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }

    /**
     * Obtiene las cabeceras 
     */
    private getHeaders() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.token);

        return headers;
    }

}
