import Dexie from 'dexie';

/**
 * Administra las conexiones y consultas a IndexedDB
 */
export class DexieService extends Dexie {
  private DB_VERSION = 1

  constructor() {
    super("almacervice");
    this.version(this.DB_VERSION)
      .stores({
        producto: '&Codigo, &Detalle',
        unidadesMedida: '&Id',
        categoria: '&Id',
        proveedor: '&Id',
        tipoDeMargen: '&Id'
      });
  }
}