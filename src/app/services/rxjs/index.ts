// Statics
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/interval';

// Operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

