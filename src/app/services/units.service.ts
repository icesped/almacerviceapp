import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Product } from '../shared/models/product';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { environment } from '../../environments/environment';
import { DexieService } from '../services/dexie.service';

@Injectable()
export class UnitsService {
    public table;
    private API_URI = environment.URI;
    private token = localStorage.getItem('jwt');

    constructor(private http: Http, private dexieService: DexieService) {
        this.table = this.dexieService.table('unidadesMedida');
    }

    /**
     * Obtiene todas las unidades de medida desde el servidor
     */
    public getAllUnitsFromServer() {
        let URL = this.API_URI + 'unidadesmedidas';
        return this.http.get(URL, { headers: this.getHeaders() })
            .map(this.extractData)
            .catch(this.handleError);
    }

    /**
     * Obtiene todas las unidades de medida almacenadas en IndexedDB
     */
    public getAllUnitsFromIndexedDB(){
        return this.table.toArray((units) => units);
    }

    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead

        return Observable.throw(errMsg);
    }

    /**
     * Extrae la data 
     * @param res 
     */
    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }

    /**
     * Obtiene las cabeceras 
     */
    private getHeaders() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.token);

        return headers;
    }

}
