import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Product } from '../shared/models/product';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { environment } from '../../environments/environment';
import { DexieService } from '../services/dexie.service';

@Injectable()
export class ProductsService {
  public table;
  private API_URI = environment.URI;
  private SUCURSAL_ID = 1 //localStorage.getItem(sucursal);
  private USER_NAME = 'Ignacio Cespedes' //localStorage.getItem(usuario);
  private token = localStorage.getItem('jwt');

  constructor(private http: Http, private dexieService: DexieService) {
    this.table = this.dexieService.table('producto');
  }

  /**
   * Obtiene todos los productos de una sucursal
   */
  getAllProductsFromServer(): Observable<Product[]> {
    let URL = this.API_URI + 'sucursales/' + this.SUCURSAL_ID + '/productos';
    return this.http.get(URL, { headers: this.getHeaders() })
      .map(this.extractData)
      .catch(this.handleError);
  }

  /**
   * Obtiene el producto base de acuerdo al codigo.
   * @param barcode 
   */
  getProductBaseFromServer(barcode): Observable<Product> {
    let URL = this.API_URI + 'productos/' + String(barcode) + '/bases';
    return this.http.get(URL, { headers: this.getHeaders() })
      .map(this.extractData)
      .catch(this.handleError);
  }

  /**
   * Obtiene un producto por medio de su Id.
   * @param id 
   */
  getProductFromServerById(id: Number) {
    let URL = this.API_URI + 'productos/' + id

    return this.http.get(URL, { headers: this.getHeaders() })
      .map(this.extractData)
      .catch(this.handleError);
  }

  /**
   * Crea un nuevo producto en la BD
   * @param product 
   */
  createProduct(product: Product): Observable<Response> {
    let URL = this.API_URI + 'productos';

    let body = {
      SucursalId: this.SUCURSAL_ID,
      Codigo: product.Codigo,
      Detalle: product.Detalle,
      Marca: product.Marca,
      Peso: product.Peso,
      UnidadMedidaId: product.UnidadMedidaId,
      CategoriaId: product.CategoriaId,
      ProveedorId: product.ProveedorId,
      StockCritico: product.StockCritico,
      Costo: product.Costo,
      Margen: product.Margen,
      TipoMargenId: product.TipoMargenId,
      Precio: product.Precio,
      UsuarioModificador: this.USER_NAME
    }

    let bodyJson = JSON.stringify(body);

    return this.http.post(URL, bodyJson, { headers: this.getHeaders() })
      .map(this.extractData)
      .catch(this.handleError);
  }

  /**
   * Actualiza un producto
   * @param id 
   * @param product 
   */
  updateProduct(id: Number, product: Product): Observable<Response> {
    let URL = this.API_URI + 'productos' + '/' + id;

    let body = {
      Detalle: product.Detalle,
      Marca: product.Marca,
      Peso: product.Peso,
      UnidadMedidaId: product.UnidadMedidaId,
      CategoriaId: product.CategoriaId,
      ProveedorId: product.ProveedorId,
      StockCritico: product.StockCritico,
      Costo: product.Costo,
      Margen: product.Margen,
      TipoMargenId: product.TipoMargenId,
      Precio: product.Precio,
      UsuarioModificador: this.USER_NAME
    }

    let bodyJson = JSON.stringify(body);

    return this.http.put(URL, bodyJson, { headers: this.getHeaders() })
      .map(this.extractData)
      .catch(this.handleError);
  }
  /**
   * Elimina un producto dado su Id.
   * @param id Producto
   */
  deleteProduct(id: Number): Observable<Response> {
    let URL = this.API_URI + 'productos' + '/' + id;

    return this.http.delete(URL, { headers: this.getHeaders() })
      .map(this.extractData)
      .catch(this.handleError);
  }

  /**
   * Obtiene un producto desde IndexedDB por medio del codigo
   * @param barCode 
   */
  getProductByBarcode(barCode: string) {
    return this.table.get(barCode)
  }

  /**
   * Busca en IndexedDB y devuelve un producto por su nombre
   * @param name 
   */
  getProductByName(name: string) {
    return this.table.where('Detalle')
      .startsWithAnyOfIgnoreCase(name)
      .toArray((products) => products);
  }

  /**
   * Obtiene todos los productos guardados en indexedDB
   */
  getAllProductsFromIndexedDB() {
    return this.table.toArray((products) => products);
  }

  /**
   * Maneja los errores y genera logs.
   */
  //TODO: GENERAR LOGS.
  private handleError(error: any) {
    let errorBody = error.json();
    let errMsg = error.status === 0 ? 'No fue posible establecer conexión con el servidor.' : errorBody.message;
    console.error(`${error.status}-${error.statusText}: ${errorBody.message}`);

    return Observable.throw(errMsg);
  }

  /**
   * Encargado de transformar las respuestas
   */
  private extractData(res: Response) {
    // Si es eliminación.
    if (res.status === 204)
      return {}
    // Otras peticiones.
    let body = res.json();
    return body.data || body || {};
  }

  /**
   * Obtiene las cabeceras 
   */
  private getHeaders() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', 'Bearer ' + this.token);

    return headers;
  }
}
