import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'thousands' })
export class ThousandsSeparatorPipe implements PipeTransform {
    transform(value: number) {
        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
}