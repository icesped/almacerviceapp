import { ValidatorFn, AbstractControl } from '@angular/forms';

export function isGreaterThanZero(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {

        let valor = control.value;
        let check = false;

        // Es mayor a cero
        if (valor > 0 || valor === '')
            check = true;
            
        return check ? null : { 'isGreaterThanZero': { valor } };
    }
}
