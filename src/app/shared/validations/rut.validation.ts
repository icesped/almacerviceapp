import { ValidatorFn, AbstractControl } from '@angular/forms';

export function isValidRut(): ValidatorFn {
    return (control: AbstractControl):{[key: string]: any} => {
        
        const rutDv = control.value;
        let check = true;
        let cuerpo;
        let dv;

        // Despejar Guión
        if(rutDv.length >= 9){
            let valor = rutDv.replace('-','');
            
            // Aislar Cuerpo y Dígito Verificador
            cuerpo = valor.slice(0,-1);
            dv = valor.slice(-1).toUpperCase();
                
            // Calcular Dígito Verificador
            let suma = 0;
            let multiplo = 2;
        
            // Para cada dígito del Cuerpo
            for(var i = 1; i <= cuerpo.length; i++) 
            {
                // Obtener su Producto con el Múltiplo Correspondiente
                let index = multiplo * +valor.charAt(cuerpo.length - i); 
            
                // Sumar al Contador General
                suma = suma + index;
            
                // Consolidar Múltiplo dentro del rango [2,7]
                if(multiplo < 7) 
                { 
                    multiplo = multiplo + 1; 
                } else { multiplo = 2; }
            }
        
            // Calcular Dígito Verificador en base al Módulo 11
            var dvEsperado = 11 - (suma % 11);
        
            // Casos Especiales (0 y K)
            dv = (dv == 'K') ? '10' : dv;
            dv = (dv == '0') ? '11' : dv;
            
            // Validar que el Cuerpo coincide con su Dígito Verificador
            if(dvEsperado.toString() != dv) 
                check = false;
        } 
        
       //Formatear RUN
       const rutDvNotPoint = cuerpo +'-'+ dv

        // Si todo sale bien, eliminar errores (decretar que es válido)
        return check ? null : {'isValidRut' : { rutDvNotPoint }};
    }
}
