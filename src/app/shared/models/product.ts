export class Product {
  public Id : number;
  public SucursalId : number;
  public Codigo : number;
  public Detalle : string;
  public Marca : string;
  public Peso : number;
  public UnidadMedidaId : number;
  public CategoriaId : number;
  public ProveedorId : number;
  public StockCritico : number;
  public NivelStock : number;
  public Costo : number;
  public Margen : number;
  public TipoMargenId : number;
  public Ganancia : number;
  public Precio : number;
  public FechaCreacion : Date;
  public EstaEliminado : boolean;
  public FechaEliminacion : Date;
  public FechaModificacion : Date;
  public Usuario : string;

  constructor(data: any = {} ) {
    this.Id  = data.Id || null;
    this.SucursalId = data.SucursalId || null;
    this.Codigo = data.Codigo || null;
    this.Detalle = data.Detalle || null;
    this.Marca = data.Marca || null;
    this.Peso = data.Peso || null;
    this.UnidadMedidaId =  data.UnidadMedidaId || null;
    this.CategoriaId = data.CategoriaId || null;
    this.ProveedorId = data.ProveedorId || null; 
    this.StockCritico = data.StockCritico || null; 
    this.NivelStock = data.NivelStock || null; 
    this.Costo = data.Costo || null; 
    this.Margen = data.Margen || null; 
    this.TipoMargenId = data.TipoMargenId || null; 
    this.Ganancia = data.Ganancia || null; 
    this.Precio = data.Precio || 0;
    this.FechaCreacion = data.FechaCreacion || null;
    this.EstaEliminado = data.EstaEliminado || false;
    this.FechaEliminacion = data.FechaEliminacion || null;
    this.FechaModificacion = data.FechaModificacion || null;
    this.Usuario = data.Usuario || null;
  }
}
