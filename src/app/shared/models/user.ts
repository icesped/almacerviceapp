export class User {
    public rut: string;
    public dv: string;
    public nombre: string;
    public password: string;
    public apellido_paterno: string;
    public apellido_materno: string;
    public email: string;

    public constructor( data: any = {}) {
        this.rut = data.rut || '';
        this.password = data.password || '';
        this.email = data.email || '';
        this.nombre = data.nombre || '';
    }

    public getName() {
        return this.nombre + ' ' + this.apellido_paterno;
    }
}