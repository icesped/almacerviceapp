export class Alerts {
    static readonly Warning = "warning";
    static readonly Success = "success";
    static readonly Info = "info";
    static readonly Error = "error";
}