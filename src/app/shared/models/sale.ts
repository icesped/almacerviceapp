export class Sale {
  public subtotal : number;
  public descuento : number;
  public total: number;

  constructor(data: any = {}) {
    this.subtotal = data.subtotal || 0;
    this.descuento = data.descuento || 0;
    this.total = data.total || 0;
  }
}
