import { Directive, HostListener, ElementRef, OnInit } from "@angular/core";
import { DecimalsPipe } from "../pipes/decimals.pipe";

@Directive({ selector: "[decimalsFormatter]" })
export class DecimalsFormatterDirective {

  private el: any;

  constructor( private elementRef: ElementRef, private decimalsPipe: DecimalsPipe ) {
    this.el = this.elementRef.nativeElement;
  }

  @HostListener("focus", ["$event.target.value"])
  onFocus(value) {
    this.el.value = this.decimalsPipe.parse(value); // opossite of transform
  }

  @HostListener("blur", ["$event.target.value"])
  onBlur(value) {
    this.el.value = (value != "") ? this.decimalsPipe.transform(value) : 0;
  }

}