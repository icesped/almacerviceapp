import { Directive, HostListener, ElementRef } from "@angular/core";

@Directive({ selector: '[onlyNumbers]' })
export class OnlyNumbersDirective {

  constructor() {
  }

  @HostListener('keypress', ['$event']) onKeyPress(event) {
    let e = <KeyboardEvent>event;
    return e.which >= 48 && e.which <= 57 ? true : false
  }
}